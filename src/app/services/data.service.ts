import { Injectable } from '@angular/core';
import { Observable, of, asapScheduler } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  Passenger =[
    {
      name: 'Nora M. Buchanan',
      avtar: 'assets/icon/avtar1.jpg',
      interactions: '4 interactions',
      email: 'invision@invisionapp.com',
      phone: '+144-3412-4422',
      location: 'New York, NY',
      paypalBtn: 'assets/icon/paypal.png',
      visaBtn: 'assets/icon/visa.png',
      mastercardBtn: 'assets/icon/mastercard.png',
      appleBtn: 'assets/icon/apple-pay.png',
      source: 'Jackson Heights',
      destination: 'Greenpoint',
      sourceAddress: '37-27 74th Street',
      destinationAddress: '81 Gate St Brooklyn',
      distance: '12.3 km',
      time: '42 min',
      price: '$34.20',
      energy:'12.4 kWh',
      isVisible: false
    },
    {
      name: 'Jessy P. Buchanan',
      avtar: 'assets/icon/avtar2.jpg',
      interactions: '5 interactions',
      email: 'jassy@invisionapp.com',
      phone: '+155-3412-5522',
      location: 'Texas, TX',
      paypalBtn: 'assets/icon/paypal.png',
      visaBtn: 'assets/icon/visa.png',
      mastercardBtn: 'assets/icon/mastercard.png',
      appleBtn: 'assets/icon/apple-pay.png',
      source: 'Buchanan Heights',
      destination: 'Jessypoint',
      sourceAddress: '35-25 75th Street',
      destinationAddress: '85 Gate St Brooklyn',
      distance: '12.5 km',
      time: '52 min',
      price: '$35.20',
      energy:'12.5 kWh',
      isVisible: false
    },
        {
      name: 'Milan K. Buchanan',
      avtar: 'assets/icon/avtar3.jpg',
      interactions: '6 interactions',
      email: 'milan@invisionapp.com',
      phone: '+166-3612-6622',
      location: 'California, CA',
      paypalBtn: 'assets/icon/paypal.png',
      visaBtn: 'assets/icon/visa.png',
      mastercardBtn: 'assets/icon/mastercard.png',
      appleBtn: 'assets/icon/apple-pay.png',
      source: 'Milan Heights',
      destination: 'Milanpoint',
      sourceAddress: '36-26 76th Street',
      destinationAddress: '86 Gate St Brooklyn',
      distance: '12.6 km',
      time: '62 min',
      price: '$36.20',
      energy:'12.6 kWh',
      isVisible: false
    },
  ];
  constructor() {}

   getPassengersData(): Observable<string[]> {
    return of(this.Passenger, asapScheduler).pipe(
        map((res: any) => res));
 }
}
