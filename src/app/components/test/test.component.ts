import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
   
  isVisible: boolean = false;
  passengers: any;

  constructor(public dataService: DataService) {}

  ngOnInit() {
    this.dataService.getPassengersData().subscribe( (result: any) => {
      console.log(result);
      this.passengers = result;
    })  
  }

  toggleVisibility(e, index){
     console.log(index); 
     this.passengers[index].isVisible = e.detail.checked;
  }
}
